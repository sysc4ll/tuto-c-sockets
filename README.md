# c-sockets - Les sockets en C

Les src du tuto https://www.ajulien.fr/c-sockets/

```text
$ make
gcc socket-getaddrinfo.c -o socket-getaddrinfo -Wall
gcc socket-connect.c -o socket-connect -Wall
gcc socket-listen.c -o socket-listen -Wall
gcc socket-listen-pthread.c -o socket-listen-pthread -Wall -lpthread
gcc socket-select.c -o socket-select -Wall
gcc socket-poll.c -o socket-poll -Wall
gcc socket-nonblock.c -o socket-nonblock -Wall
gcc socket-win-linux.c -o socket-win-linux -Wall
gcc socket-udp-recvfrom.c -o socket-udp-recvfrom -Wall
gcc socket-udp-sendto.c -o socket-udp-sendto -Wall
```
