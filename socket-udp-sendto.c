#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
    int port;
    struct sockaddr_in sin;
    int sock;

    if (argc != 4)
    {
        fprintf(stderr, "usage : %s <ip> <port> <message>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    errno = 0;
    port = strtol(argv[2], NULL, 10);

    if (errno != 0)
    {
        perror("strtol()");
        exit(EXIT_FAILURE);
    }
    
    memset(&sin, 0, sizeof(struct sockaddr_in));

    sin.sin_addr.s_addr = inet_addr(argv[1]);
    sin.sin_port = htons(port); 
    sin.sin_family = AF_INET; /* IPv4 only */

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    if (sendto(sock, argv[3], strlen(argv[3]), 0, (struct sockaddr *)&sin, sizeof(struct sockaddr)) == -1)
    {
        perror("sendto()");
        exit(EXIT_FAILURE);
    }

    return 0;
}

