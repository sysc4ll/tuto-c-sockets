#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
    int port;
    struct sockaddr_in sin;
    int sock;
    struct sockaddr_in csin; /* client */
    socklen_t csin_size;
    int rv;
    char buffer[1024 + 1];

    if (argc != 2)
    {
        fprintf(stderr, "usage : %s <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    errno = 0;
    port = strtol(argv[1], NULL, 10);

    if (errno != 0)
    {
        perror("strtol()");
        exit(EXIT_FAILURE);
    }

    memset(&sin, 0, sizeof(struct sockaddr_in));

    sin.sin_addr.s_addr = htonl(INADDR_ANY); /* my IPv4 address */
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET; /* IPv4 only */

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    if (bind(sock, (struct sockaddr *)&sin, sizeof(struct sockaddr)) == -1)
    {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    memset(&csin, 0, sizeof(struct sockaddr_in));
    csin_size = sizeof(struct sockaddr_in);

    if ((rv = recvfrom(sock, buffer, 1024, 0, (struct sockaddr *)&csin, &csin_size)) == -1)
    {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }

    buffer[rv] = 0;

    printf("received from UDP socket : %s\n", buffer);

    return 0;
}

